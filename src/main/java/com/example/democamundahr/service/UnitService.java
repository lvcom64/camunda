package com.example.democamundahr.service;

import com.example.democamundahr.rest.UnitHeadRequest;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;


public class UnitService {
    private static RestTemplate restTemplate = new RestTemplate();
    private static ObjectMapper objectMapper = new ObjectMapper();
    private static HttpHeaders httpHeaders = new HttpHeaders();

    private UnitService() {
        throw new IllegalStateException("Utility class");
    }

    public static String getUnitHead(int unitId) throws JsonProcessingException {
        //httpHeaders.setContentType(MediaType.APPLICATION_JSON);
        String getUnitUrl = "http://localhost:8280/api/v2/units/{unitId}";
        ResponseEntity<String> response = restTemplate.getForEntity(getUnitUrl, String.class, unitId);
        JsonNode root = objectMapper.readTree(response.getBody());
        return root.path("headId").asText();
    }

    public static void updateUnitHead(UnitHeadRequest unitHeadRequest, int unitId) throws JsonProcessingException {
        httpHeaders.setContentType(MediaType.APPLICATION_JSON);
        String postUnitHeadUrl = "http://localhost:8280/api/v2/units/{unitId}";
        restTemplate.put(postUnitHeadUrl, unitHeadRequest, unitId);
    }

}
